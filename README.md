# SimpleATMServerClient

Uses .txt files (Supports Swedish, English & Spanish) and messages composed of single digits to communicate between server and client. 
Compile and run instance of each, starting with Server. Clients can be multiple. 
 
java ATMServer
java ATMClient 127.0.0.1
 
Credentials are hard-coded:
 
Card Number | Passcode

1111 | 1234

1112 | 12345

1113 | 123456
 
Security codes: 01,03,05,07,09,11..99